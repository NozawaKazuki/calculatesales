package jp.alhinc.nozawa_kazuki.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	public static void main(String[] args) {
		//Mapの宣言
		Map<String, String> branchNames = new HashMap<String, String>();
		Map<String, Long> branchSales = new HashMap<String, Long>();
		Map<String, String> commodityNames = new HashMap<String, String>();
		Map<String, Long> commoditySales = new HashMap<String, Long>();
		//コマンドライン引数が１つ渡されなかった場合の処理
		if (args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
		}
		//入力 返ってきた値がfalseなら終了
		if (!input(args[0], "branch.lst", "[0-9]{3}+$", branchNames, branchSales, "支店")) {
			return;
		}
		if (!input(args[0], "commodity.lst", "[0-9a-zA-Z]{8}+$", commodityNames, commoditySales, "商品")) {
			return;
		}
		//集計処理 返ってきた値がfalseなら終了
		if (!aggregate(args[0], branchNames, branchSales, commodityNames, commoditySales)) {
			return;
		}

		//出力 返ってきた値がfalseなら終了
		if (!output(args[0], "branch.out", branchNames, branchSales)) {
			return;
		}
		if (!output(args[0], "commodity.out", commodityNames, commoditySales)) {
			return;
		}
	}

	//入力のメソッド
	private static boolean input(String filePlace, String fileName, String match, Map<String, String> name,
			Map<String, Long> sale, String error) {
		BufferedReader br = null;
		try {
			File file = new File(filePlace, fileName);
			//ファイルがない場合の処理
			if (!file.exists()) {
				System.out.println(error + "定義ファイルが存在しません");
				return false;
			}
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			String line;
			while ((line = br.readLine()) != null) {
				String[] items = line.split(",");//文字列の分割
				//要素数が2でない、または支店コードが数字3桁でない場合の処理
				//要素数が2でない、または支店コードが英数字8桁でない場合の処理
				if ((items.length != 2) || (!items[0].matches(match))) {
					System.out.println(error + "定義ファイルのフォーマットが不正です");
					return false;
				}
				name.put(items[0], items[1]);//Mapに２つの要素を追加
				sale.put(items[0], 0L);
			}

		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if (br != null) {//もし読み込みができなければ
				try {
					br.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}

	//集計のメソッド
	private static boolean aggregate(String filePlace, Map<String, String> branchNames, Map<String, Long> branchSales,
			Map<String, String> commodityNames, Map<String, Long> commoditySales) {
		BufferedReader br = null;
		//8桁の数字＋rcdのファイルを検索する
		File lib = new File(filePlace);
		File[] files = lib.listFiles();//全ファイルの取得
		List<File> rcdFiles = new ArrayList<File>();
		Collections.sort(rcdFiles);//売上ファイルのソート
		//ファイルの追加
		for (int i = 0; i < files.length; i++) {
			String fileName = files[i].getName();
			if (files[i].isFile() && fileName.matches("^[0-9]{8}.rcd+$")) {//ファイルかつ8桁の数字であることを判定
				rcdFiles.add(files[i]);//ファイルの追加
			}
		}
		//ファイル名が連番であることを確認
		for (int i = 0; i < rcdFiles.size() - 1; i++) {
			//先頭から8文字切り出しint型へ変換
			int former = Integer.parseInt(files[i].getName().substring(0, 8));
			int latter = Integer.parseInt(files[i + 1].getName().substring(0, 8));
			if ((latter - former) != 1) {
				System.out.println("売上ファイル名が連番になっていません");
				return false;
			}
		}
		//検索したファイルの中身を読み、追加
		for (int i = 0; i < rcdFiles.size(); i++) {
			try {
				ArrayList<String> earningslist = new ArrayList<String>();
				FileReader fr = new FileReader(rcdFiles.get(i));
				br = new BufferedReader(fr);
				String line;
				while ((line = br.readLine()) != null) {
					earningslist.add(line);
				}
				//売上ファイルが3行でなかった場合の処理
				if (earningslist.size() != 3) {
					System.out.println(rcdFiles.get(i).getName() + "のフォーマットが不正です");
					return false;
				}
				//2行目に追加された値が8桁の英数字なのか確認
				if (!earningslist.get(1).matches("[0-9a-zA-Z]{8}+")) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
				//3行目に追加された値が数字なのか確認
				if (!earningslist.get(2).matches("^[0-9]+$")) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
				//支店コードが支店定義ファイルに存在しない場合の処理
				if (!branchNames.containsKey(earningslist.get(0))) {
					System.out.println(rcdFiles.get(i).getName() + "の支店コードが不正です");
					return false;
				}
				//追加した数値をStringからlongへ変換、加算
				long fileSale = Long.parseLong(earningslist.get(2));
				Long saleAmount = branchSales.get(earningslist.get(0)) + fileSale;
				Long saleAmountc = commoditySales.get(earningslist.get(1)) + fileSale;

				//合計金額が11桁以上の場合の処理
				if (saleAmount >= 10000000000L || saleAmountc >= 10000000000L) {
					System.out.println("合計金額が10桁を超えました");
					return false;
				}
				commoditySales.put(earningslist.get(1), saleAmountc);//commoditySalesに追加
				branchSales.put(earningslist.get(0), saleAmount);//branchSalesに追加

			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return false;
			} finally {
				if (br != null) {//もし読み込みができなければ
					try {
						br.close();
					} catch (IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return false;
					}
				}
			}
		}

		return true;
	}

	//出力のメソッド
	private static boolean output(String filePlace, String fileName, Map<String, String> name, Map<String, Long> sale) {

		BufferedWriter bw = null;
		try {
			File file = new File(filePlace, fileName);
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);
			//Mapからkeyの取得
			for (String key : name.keySet()) {
				String names = name.get(key);
				Long sales = sale.get(key);
				bw.write(key + "," + names + "," + sales.toString());
				bw.newLine();
			}
		} catch (IOException e) {//ファイルが存在しない
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if (bw != null) {//もし読み込みができなければ
				try {
					bw.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}

}
